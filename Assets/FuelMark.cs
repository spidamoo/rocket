﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelMark : MonoBehaviour
{
    Rocket rocket;
    // Start is called before the first frame update
    void Start()
    {
        rocket = GetComponentInParent<Rocket>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(transform.localScale.x, rocket.GetFuelTimer(), transform.localScale.z);
    }
}
