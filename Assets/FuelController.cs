﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelController : MonoBehaviour
{
    static Color hoverColor = new Color(0.717f, 0.478f, 0.023f);
    static Color normalColor = new Color(0.666f, 0.290f, 0.039f);

    public enum ControllerSide
    {
        Start, End
    };
    public ControllerSide side;
    public EngineController engine;

    SpriteRenderer m_SpriteRenderer;
    Vector3 dragPoint = Vector3.positiveInfinity;
    // Start is called before the first frame update
    void Start()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_SpriteRenderer.color = normalColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseEnter()
    {
        m_SpriteRenderer.color = hoverColor;
    }

    void OnMouseExit()
    {
        m_SpriteRenderer.color = normalColor;
    }

    void OnMouseDrag()
    {
        if ( Vector3.Equals(dragPoint, Vector3.positiveInfinity) )
        {
            dragPoint = Camera.main.ScreenToWorldPoint( new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.localPosition.z) ) - transform.localPosition;
        }
        else
        {
            Vector3 newDragPoint = Camera.main.ScreenToWorldPoint( new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.localPosition.z) );
            float newY = (newDragPoint - dragPoint).y;

            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || true)
            {
                newY = (float)Math.Round(newY, 1);
            }

            float maxY = side == ControllerSide.Start ?  0.5f : engine.fuelStart - 0.5f;
            float minY = side == ControllerSide.End   ? -0.5f : engine.fuelEnd   - 0.5f;
            if (newY > maxY) newY = maxY;
            if (newY < minY) newY = minY;

            if (side == ControllerSide.Start)
            {
                engine.fuelStart = newY + 0.5f;
            }
            else
            {
                engine.fuelEnd   = newY + 0.5f;
            }

            transform.localPosition = new Vector3(transform.localPosition.x, newY, transform.localPosition.z);
        }
    }
    void OnMouseUp()
    {
        dragPoint = Vector3.positiveInfinity;
    }
}
