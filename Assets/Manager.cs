﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Manager : MonoBehaviour
{
    public static Manager instance;
    public Checkpoint lastCheckpoint;
    public Rocket rocket;
    public GameObject constructionGUI;
    public GameObject launchedGUI;
    // Start is called before the first frame update
    void Start()
    {
        constructionGUI.SetActive(true);
        launchedGUI.SetActive(false);
        lastCheckpoint.levelCamera.Priority = 9;

        Manager.instance = this;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Launch()
    {
        constructionGUI.SetActive(false);
        launchedGUI.SetActive(true);
        lastCheckpoint.levelCamera.Priority = 11;
        rocket.Launch();
    }
    public void Stop()
    {
        constructionGUI.SetActive(true);
        launchedGUI.SetActive(false);
        lastCheckpoint.levelCamera.Priority = 9;
        rocket.Stop(lastCheckpoint.transform.position);
    }
    public void PassCheckpoint(Checkpoint checkPoint)
    {
        lastCheckpoint.levelCamera.Priority = 9;
        lastCheckpoint = checkPoint;
        lastCheckpoint.levelCamera.Priority = 11;
    }
}
