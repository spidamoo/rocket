﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineController : MonoBehaviour
{
    static Color normalColor = new Color(0.1411765f, 0.5764706f, 0.2156863f);
    static Color hoverColor = new Color(0.419f, 0.894f, 0.015f);
    static Color selectedColor = new Color(0.776f, 0.870f, 0.470f);

    public Vector2 force = Vector2.up;
    public float fuelStart = 1.0f;
    public float fuelEnd   = 0.0f;

    SpriteRenderer sprite;
    Vector3 dragPoint = Vector3.positiveInfinity;
    private bool selected;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        sprite.color = normalColor;

        foreach (FuelController arrow in transform.parent.GetComponentsInChildren<FuelController>() )
        {
            arrow.gameObject.SetActive(false);
        }
    }

    void Update()
    {
        
    }

    void OnMouseEnter()
    {
        if (!selected)
        {
            sprite.color = hoverColor;
        }
        else
        {
            sprite.color = selectedColor;
        }
    }

    void OnMouseExit()
    {
        if (!selected)
        {
            sprite.color = normalColor;
        }
        else
        {
            sprite.color = selectedColor;
        }
    }

    void OnMouseDrag()
    {
        if ( Vector3.Equals(dragPoint, Vector3.positiveInfinity) )
        {
            dragPoint = Camera.main.ScreenToWorldPoint( new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.localPosition.z) ) - transform.localPosition;
        }
        else
        {
            Vector3 newDragPoint = Camera.main.ScreenToWorldPoint( new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.localPosition.z) );
            float newY = (newDragPoint - dragPoint).y;
            float newX = (newDragPoint - dragPoint).x;

            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || true)
            {
                newX = (float)Math.Round(newX, 1);
                newY = (float)Math.Round(newY, 1);
            }

            float maxY =  0.5f;
            float minY = -0.5f;
            float maxX =  0.5f;
            float minX = -0.5f;
            if (newY > maxY) newY = maxY;
            if (newY < minY) newY = minY;
            if (newX > maxX) newX = maxX;
            if (newX < minX) newX = minX;


            transform.localPosition = new Vector3(newX, newY, transform.localPosition.z);
        }
    }
    void OnMouseUp()
    {
        dragPoint = Vector3.positiveInfinity;
    }

    void OnMouseDown()
    {
        Select();
    }

    public void Launch()
    {
    }
    public void Stop()
    {
    }

    public void Select()
    {
        EngineController.DeselectAll();
        foreach (FuelController arrow in transform.parent.GetComponentsInChildren<FuelController>(true) )
        {
            arrow.gameObject.SetActive(true);
        }
        selected = true;
        sprite.color = selectedColor;
    }
    public void Deselect()
    {
        selected = false;
        foreach (FuelController arrow in transform.parent.GetComponentsInChildren<FuelController>(true) )
        {
            arrow.gameObject.SetActive(false);
        }
        sprite.color = normalColor;
    }
    public static void DeselectAll()
    {
        foreach (EngineController engine in GameObject.FindObjectsOfType<EngineController>() )
        {
            engine.Deselect();
        }
    }

    public bool IsSelected()
    {
        return selected;
    }
}
