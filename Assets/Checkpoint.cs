﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Checkpoint : MonoBehaviour
{
    static Color checkedColor = new Color(0.2431373f, 0.254902f, 0.372549f);

    public bool isChecked;
    public CinemachineVirtualCamera levelCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Check()
    {
        isChecked = true;
        GetComponent<SpriteRenderer>().color = checkedColor;
        GetComponent<AudioSource>().Play();
        Manager.instance.PassCheckpoint(this);
    }

    void OnTriggerEnter2D()
    {
        if (!isChecked)
        {
            Check();
        }
    }
}
