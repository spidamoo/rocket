﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flame : MonoBehaviour
{
    static Color startColor = new Color(0.952f, 0.760f, 0.125f);
    static Color endColor = new Color(0.709f, 0.254f, 0.192f, 0.0f);
    private float ttl = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().color = startColor;
    }

    // Update is called once per frame
    void Update()
    {
        ttl -= Time.deltaTime;
        if (ttl < 0.0f)
        {
            GameObject.Destroy(gameObject);
        }

        GetComponent<SpriteRenderer>().color = ( startColor * ttl + endColor * (5.0f - ttl) ) / 5;
    }
}
