﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    public GameObject flame;
    public float flameInterval = 0.1f;

    bool launched = false;
    float fuelTimer = 1.0f;
    float flameTimer = 0.0f;
    Rigidbody2D body;
    Hull hull;
    AudioSource engineSound;
    AudioSource bumpSound;
    // Start is called before the first frame update
    void Start()
    {
        engineSound = GameObject.Find("Rocket/EngineSound").GetComponent<AudioSource>();
        bumpSound   = GameObject.Find("Rocket/BumpSound").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (launched && hull)
        {
            fuelTimer += -Time.deltaTime * 0.1f;
            if (fuelTimer < 0.0f)
                fuelTimer = -0.001f;
            flameTimer -= Time.deltaTime;

            bool enginesWorking = false;
            foreach (EngineController engine in GetComponentsInChildren<EngineController>() )
            {
                if (fuelTimer > engine.fuelStart || fuelTimer < engine.fuelEnd)
                    continue;

                enginesWorking = true;

                body.AddForceAtPosition(
                    transform.TransformDirection(engine.force),
                    engine.transform.position
                );

                if (flameTimer < 0.0f)
                {
                    var newFlame = Instantiate(flame);
                    newFlame.transform.SetPositionAndRotation(
                        engine.transform.position + (Vector3)(Random.insideUnitCircle * 0.2f),
                        Quaternion.identity
                    );
                    newFlame.transform.localScale += (Vector3)(Random.insideUnitCircle * 0.2f);
                    newFlame.GetComponent<Rigidbody2D>().AddForceAtPosition(
                        -0.05f * transform.TransformDirection(engine.force),
                        engine.transform.position + (Vector3)(Random.insideUnitCircle * 0.2f),
                        ForceMode2D.Impulse
                    );
                }
            }

            if (enginesWorking && !engineSound.isPlaying)
            {
                engineSound.Play();
            }
            if (!enginesWorking && engineSound.isPlaying)
            {
                engineSound.Stop();
            }

            if (flameTimer < 0.0f)
            {
                flameTimer += flameInterval;
            }
        }
    }

    public void Launch()
    {
        launched = true;

        EngineController.DeselectAll();
        foreach (EngineController engine in GetComponentsInChildren<EngineController>() )
        {
            engine.Launch();
        }

        body = gameObject.AddComponent<Rigidbody2D>();
        body.useAutoMass = true;
        body.drag = 0.15f;
        body.angularDrag = 0.15f;

        hull = GetComponentInChildren<Hull>();
    }
    public void Stop(Vector3 resetPosition)
    {
        launched = false;

        foreach (EngineController engine in GetComponentsInChildren<EngineController>() )
        {
            engine.Stop();
        }

        Component.Destroy(body);

        transform.SetPositionAndRotation(resetPosition, Quaternion.identity);
        fuelTimer = 1.0f;

        engineSound.Stop();
    }

    public void AddElement(GameObject prefab)
    {
        var newElement = Instantiate(prefab, this.transform);
        // newElement.GetComponentInChildren<EngineController>().Select();
    }
    public void DeleteElement()
    {
        foreach (EngineController engine in GameObject.FindObjectsOfType<EngineController>() )
        {
            if ( engine.IsSelected() )
            {
                GameObject.Destroy(engine.transform.parent.gameObject);
            }
        }
    }
    public void AddHull(GameObject prefab)
    {
        DeleteHull();
        Instantiate(prefab, this.transform);
    }
    public void DeleteHull()
    {
        foreach (Hull hull in GameObject.FindObjectsOfType<Hull>() )
        {
            GameObject.Destroy(hull.gameObject);
        }
    }

    public float GetFuelTimer()
    {
        return fuelTimer;
    }

    void OnCollisionEnter2D()
    {
        bumpSound.Play();
    }
}
